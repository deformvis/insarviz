Running all the tests
---------------------

Testing can be done by running *pytest*. Navigate to the main insarviz folder, and run the test with the following command: 

.. code-block :: bash
    
        pytest
