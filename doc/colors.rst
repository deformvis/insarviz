Colormaps
==============

| Where is the rainbow colormap in InsarViz? 
|
| Well...
|
| In the words of F. Crameri et al. (2020)::
|
|	The accurate representation of data is essential in science communication. However, 	colour maps that visually distort data through uneven colour gradients or are 
	unreadable to those with colour-vision deficiency remain prevalent in science. These 	include, but are not limited to, rainbow-like and red–green colour maps.
|
| Insarviz offers a range of color colormaps carefully designed to be **perceptually uniform and ordered**, to **represent data fairly, without visual distortion**, and to ensure **universal readability**.
|
| For this, we rely on the works of Crameri and others:
| `Scientific colour maps <https://www.fabiocrameri.ch/colourmaps/>`_ \
| `The misuse of colour in science communication <https://www.nature.com/articles/s41467-020-19160-7>`_
