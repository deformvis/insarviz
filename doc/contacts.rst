Contacts, bug reports, contributing to the code
===============================================

If you have any problem or any request, you can contact the 
the developers using the following mailing list : insarviz-sos@univ-grenoble-alpes.fr

If you want to stay tuned, you can register to the insarviz-info mailing list 
by sending an email to insarviz-notification@univ-grenoble-alpes.fr with the subject 

"subscribe insarviz-notification@univ-grenoble-alpes.fr FIRST_NAME LAST_NAME" 

The traffic of this mailing list is very low (at most a few emails per year) but let you know when new
releases are made. 


We welcome contributions to the project. For this, you will have to create an account
on our gitlab (https://register.gricad-gitlab.univ-grenoble-alpes.fr/index.html).
Get in touch with us via the insarviz-sos@univ-grenoble-alpes.fr mailing list so we can work 
together and improve InsarViz !
