.. image:: images/logo-uga.png
   :width: 30pt

.. image:: images/logo-cnes-points.png
   :width: 30pt

.. image:: images/logo_cnrs_small.png
   :width: 30pt

InsarViz
********

.. toctree::
    :maxdepth: 2
    :caption: Documentation

    why
    installation
    supported
    tutorial
    colors
    istoar
    test
    contacts
    citation
    authors
    funding
    changes
    modules
    
  
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

