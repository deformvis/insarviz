How to cite
===========

If you use InsarViz for your project, please consider citing `this paper <https://doi.org/10.21105/joss.06440>`_
