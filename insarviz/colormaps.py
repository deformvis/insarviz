#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Colormap

Perceptually uniform, perceptually ordered, colour-deficiency and
colour-blind friendly, readable in black and white prints.

From the works of F. Crameri
https://www.fabiocrameri.ch/colourmaps/

"""

from PySide6.QtOpenGL import QOpenGLTexture

import numpy as np

from pyqtgraph.colormap import ColorMap

from pyqtgraph.graphicsItems.GradientEditorItem import Gradients

grey = [[0., 0., 0.],
        [1., 1., 1.]]

batlow_cm = [[0.0051932, 0.098238, 0.34984],
             [0.0090652, 0.10449, 0.35093],
             [0.012963, 0.11078, 0.35199],
             [0.01653, 0.11691, 0.35307],
             [0.019936, 0.12298, 0.35412],
             [0.023189, 0.12904, 0.35518],
             [0.026291, 0.13504, 0.35621],
             [0.029245, 0.14096, 0.35724],
             [0.032053, 0.14677, 0.35824],
             [0.034853, 0.15256, 0.35923],
             [0.037449, 0.15831, 0.36022],
             [0.039845, 0.16398, 0.36119],
             [0.042104, 0.16956, 0.36215],
             [0.044069, 0.17505, 0.36308],
             [0.045905, 0.18046, 0.36401],
             [0.047665, 0.18584, 0.36491],
             [0.049378, 0.19108, 0.36581],
             [0.050795, 0.19627, 0.36668],
             [0.052164, 0.20132, 0.36752],
             [0.053471, 0.20636, 0.36837],
             [0.054721, 0.21123, 0.36918],
             [0.055928, 0.21605, 0.36997],
             [0.057033, 0.22075, 0.37075],
             [0.058032, 0.22534, 0.37151],
             [0.059164, 0.22984, 0.37225],
             [0.060167, 0.2343, 0.37298],
             [0.061052, 0.23862, 0.37369],
             [0.06206, 0.24289, 0.37439],
             [0.063071, 0.24709, 0.37505],
             [0.063982, 0.25121, 0.37571],
             [0.064936, 0.25526, 0.37636],
             [0.065903, 0.25926, 0.37699],
             [0.066899, 0.26319, 0.37759],
             [0.067921, 0.26706, 0.37819],
             [0.069002, 0.27092, 0.37877],
             [0.070001, 0.27471, 0.37934],
             [0.071115, 0.2785, 0.37989],
             [0.072192, 0.28225, 0.38043],
             [0.07344, 0.28594, 0.38096],
             [0.074595, 0.28965, 0.38145],
             [0.075833, 0.29332, 0.38192],
             [0.077136, 0.297, 0.38238],
             [0.078517, 0.30062, 0.38281],
             [0.079984, 0.30425, 0.38322],
             [0.081553, 0.30786, 0.3836],
             [0.083082, 0.31146, 0.38394],
             [0.084778, 0.31504, 0.38424],
             [0.086503, 0.31862, 0.38451],
             [0.088353, 0.32217, 0.38473],
             [0.090281, 0.32569, 0.38491],
             [0.092304, 0.32922, 0.38504],
             [0.094462, 0.33271, 0.38512],
             [0.096618, 0.33616, 0.38513],
             [0.099015, 0.33962, 0.38509],
             [0.10148, 0.34304, 0.38498],
             [0.10408, 0.34641, 0.3848],
             [0.10684, 0.34977, 0.38455],
             [0.1097, 0.3531, 0.38422],
             [0.11265, 0.35639, 0.38381],
             [0.11575, 0.35964, 0.38331],
             [0.11899, 0.36285, 0.38271],
             [0.12232, 0.36603, 0.38203],
             [0.12589, 0.36916, 0.38126],
             [0.12952, 0.37224, 0.38038],
             [0.1333, 0.37528, 0.3794],
             [0.13721, 0.37828, 0.37831],
             [0.14126, 0.38124, 0.37713],
             [0.14543, 0.38413, 0.37584],
             [0.14971, 0.38698, 0.37445],
             [0.15407, 0.38978, 0.37293],
             [0.15862, 0.39253, 0.37132],
             [0.16325, 0.39524, 0.36961],
             [0.16795, 0.39789, 0.36778],
             [0.17279, 0.4005, 0.36587],
             [0.17775, 0.40304, 0.36383],
             [0.18273, 0.40555, 0.36171],
             [0.18789, 0.408, 0.35948],
             [0.19305, 0.41043, 0.35718],
             [0.19831, 0.4128, 0.35477],
             [0.20368, 0.41512, 0.35225],
             [0.20908, 0.41741, 0.34968],
             [0.21455, 0.41966, 0.34702],
             [0.22011, 0.42186, 0.34426],
             [0.22571, 0.42405, 0.34146],
             [0.23136, 0.4262, 0.33857],
             [0.23707, 0.42832, 0.33563],
             [0.24279, 0.43042, 0.33263],
             [0.24862, 0.43249, 0.32957],
             [0.25445, 0.43453, 0.32643],
             [0.26032, 0.43656, 0.32329],
             [0.26624, 0.43856, 0.32009],
             [0.27217, 0.44054, 0.31683],
             [0.27817, 0.44252, 0.31355],
             [0.28417, 0.44448, 0.31024],
             [0.29021, 0.44642, 0.30689],
             [0.29629, 0.44836, 0.30351],
             [0.30238, 0.45028, 0.30012],
             [0.30852, 0.4522, 0.29672],
             [0.31465, 0.45411, 0.29328],
             [0.32083, 0.45601, 0.28984],
             [0.32701, 0.4579, 0.28638],
             [0.33323, 0.45979, 0.28294],
             [0.33947, 0.46168, 0.27947],
             [0.3457, 0.46356, 0.276],
             [0.35198, 0.46544, 0.27249],
             [0.35828, 0.46733, 0.26904],
             [0.36459, 0.46921, 0.26554],
             [0.37092, 0.47109, 0.26206],
             [0.37729, 0.47295, 0.25859],
             [0.38368, 0.47484, 0.25513],
             [0.39007, 0.47671, 0.25166],
             [0.3965, 0.47859, 0.24821],
             [0.40297, 0.48047, 0.24473],
             [0.40945, 0.48235, 0.24131],
             [0.41597, 0.48423, 0.23789],
             [0.42251, 0.48611, 0.23449],
             [0.42909, 0.48801, 0.2311],
             [0.43571, 0.48989, 0.22773],
             [0.44237, 0.4918, 0.22435],
             [0.44905, 0.49368, 0.22107],
             [0.45577, 0.49558, 0.21777],
             [0.46254, 0.4975, 0.21452],
             [0.46937, 0.49939, 0.21132],
             [0.47622, 0.50131, 0.20815],
             [0.48312, 0.50322, 0.20504],
             [0.49008, 0.50514, 0.20198],
             [0.49709, 0.50706, 0.19899],
             [0.50415, 0.50898, 0.19612],
             [0.51125, 0.5109, 0.1933],
             [0.51842, 0.51282, 0.19057],
             [0.52564, 0.51475, 0.18799],
             [0.53291, 0.51666, 0.1855],
             [0.54023, 0.51858, 0.1831],
             [0.5476, 0.52049, 0.18088],
             [0.55502, 0.52239, 0.17885],
             [0.56251, 0.52429, 0.17696],
             [0.57002, 0.52619, 0.17527],
             [0.57758, 0.52806, 0.17377],
             [0.5852, 0.52993, 0.17249],
             [0.59285, 0.53178, 0.17145],
             [0.60052, 0.5336, 0.17065],
             [0.60824, 0.53542, 0.1701],
             [0.61597, 0.53723, 0.16983],
             [0.62374, 0.539, 0.16981],
             [0.63151, 0.54075, 0.17007],
             [0.6393, 0.54248, 0.17062],
             [0.6471, 0.54418, 0.17146],
             [0.65489, 0.54586, 0.1726],
             [0.66269, 0.5475, 0.17404],
             [0.67048, 0.54913, 0.17575],
             [0.67824, 0.55071, 0.1778],
             [0.686, 0.55227, 0.18006],
             [0.69372, 0.5538, 0.18261],
             [0.70142, 0.55529, 0.18548],
             [0.7091, 0.55677, 0.18855],
             [0.71673, 0.5582, 0.19185],
             [0.72432, 0.55963, 0.19541],
             [0.73188, 0.56101, 0.19917],
             [0.73939, 0.56239, 0.20318],
             [0.74685, 0.56373, 0.20737],
             [0.75427, 0.56503, 0.21176],
             [0.76163, 0.56634, 0.21632],
             [0.76894, 0.56763, 0.22105],
             [0.77621, 0.5689, 0.22593],
             [0.78342, 0.57016, 0.23096],
             [0.79057, 0.57142, 0.23616],
             [0.79767, 0.57268, 0.24149],
             [0.80471, 0.57393, 0.24696],
             [0.81169, 0.57519, 0.25257],
             [0.81861, 0.57646, 0.2583],
             [0.82547, 0.57773, 0.2642],
             [0.83227, 0.57903, 0.27021],
             [0.839, 0.58034, 0.27635],
             [0.84566, 0.58167, 0.28263],
             [0.85225, 0.58304, 0.28904],
             [0.85875, 0.58444, 0.29557],
             [0.86517, 0.58588, 0.30225],
             [0.87151, 0.58735, 0.30911],
             [0.87774, 0.58887, 0.31608],
             [0.88388, 0.59045, 0.32319],
             [0.8899, 0.59209, 0.33045],
             [0.89581, 0.59377, 0.33787],
             [0.90159, 0.59551, 0.34543],
             [0.90724, 0.59732, 0.35314],
             [0.91275, 0.59919, 0.36099],
             [0.9181, 0.60113, 0.369],
             [0.9233, 0.60314, 0.37714],
             [0.92832, 0.60521, 0.3854],
             [0.93318, 0.60737, 0.39382],
             [0.93785, 0.60958, 0.40235],
             [0.94233, 0.61187, 0.41101],
             [0.94661, 0.61422, 0.41977],
             [0.9507, 0.61665, 0.42862],
             [0.95457, 0.61914, 0.43758],
             [0.95824, 0.62167, 0.4466],
             [0.9617, 0.62428, 0.4557],
             [0.96494, 0.62693, 0.46486],
             [0.96798, 0.62964, 0.47406],
             [0.9708, 0.63239, 0.48329],
             [0.97342, 0.63518, 0.49255],
             [0.97584, 0.63801, 0.50183],
             [0.97805, 0.64087, 0.51109],
             [0.98008, 0.64375, 0.52035],
             [0.98192, 0.64666, 0.5296],
             [0.98357, 0.64959, 0.53882],
             [0.98507, 0.65252, 0.548],
             [0.98639, 0.65547, 0.55714],
             [0.98757, 0.65842, 0.56623],
             [0.9886, 0.66138, 0.57526],
             [0.9895, 0.66433, 0.58425],
             [0.99027, 0.66728, 0.59317],
             [0.99093, 0.67023, 0.60203],
             [0.99148, 0.67316, 0.61084],
             [0.99194, 0.67609, 0.61958],
             [0.9923, 0.67901, 0.62825],
             [0.99259, 0.68191, 0.63687],
             [0.99281, 0.68482, 0.64542],
             [0.99297, 0.68771, 0.65393],
             [0.99306, 0.69058, 0.6624],
             [0.99311, 0.69345, 0.67081],
             [0.99311, 0.69631, 0.67918],
             [0.99307, 0.69916, 0.68752],
             [0.993, 0.70201, 0.69583],
             [0.9929, 0.70485, 0.70411],
             [0.99277, 0.70769, 0.71238],
             [0.99262, 0.71053, 0.72064],
             [0.99245, 0.71337, 0.72889],
             [0.99226, 0.71621, 0.73715],
             [0.99205, 0.71905, 0.7454],
             [0.99184, 0.72189, 0.75367],
             [0.99161, 0.72475, 0.76196],
             [0.99137, 0.72761, 0.77027],
             [0.99112, 0.73049, 0.77861],
             [0.99086, 0.73337, 0.78698],
             [0.99059, 0.73626, 0.79537],
             [0.99031, 0.73918, 0.80381],
             [0.99002, 0.7421, 0.81229],
             [0.98972, 0.74504, 0.8208],
             [0.98941, 0.748, 0.82937],
             [0.98909, 0.75097, 0.83798],
             [0.98875, 0.75395, 0.84663],
             [0.98841, 0.75695, 0.85533],
             [0.98805, 0.75996, 0.86408],
             [0.98767, 0.763, 0.87286],
             [0.98728, 0.76605, 0.8817],
             [0.98687, 0.7691, 0.89057],
             [0.98643, 0.77218, 0.89949],
             [0.98598, 0.77527, 0.90845],
             [0.9855, 0.77838, 0.91744],
             [0.985, 0.7815, 0.92647],
             [0.98447, 0.78462, 0.93553],
             [0.98391, 0.78776, 0.94463],
             [0.98332, 0.79091, 0.95375],
             [0.9827, 0.79407, 0.9629],
             [0.98205, 0.79723, 0.97207],
             [0.98135, 0.80041, 0.98127]]

#         vik
vik_cm = [[0.0013282, 0.069836, 0.37953],
          [0.0023664, 0.076475, 0.38352],
          [0.0033042, 0.083083, 0.38749],
          [0.0041459, 0.08959, 0.39148],
          [0.0048968, 0.095948, 0.39545],
          [0.0055632, 0.10227, 0.39941],
          [0.0061512, 0.1085, 0.40339],
          [0.0066676, 0.11469, 0.40734],
          [0.0071192, 0.12085, 0.41129],
          [0.0075116, 0.12696, 0.41523],
          [0.0078503, 0.13307, 0.41917],
          [0.0081413, 0.13909, 0.42308],
          [0.0083912, 0.14517, 0.42701],
          [0.0086057, 0.15114, 0.43091],
          [0.0087895, 0.15714, 0.43481],
          [0.0089466, 0.16315, 0.43869],
          [0.0090802, 0.16914, 0.44259],
          [0.0091935, 0.1751, 0.44646],
          [0.0092897, 0.18105, 0.45034],
          [0.0093719, 0.18705, 0.45421],
          [0.0094431, 0.19303, 0.45808],
          [0.0095061, 0.199, 0.46195],
          [0.0095639, 0.20501, 0.46582],
          [0.0096192, 0.21102, 0.46971],
          [0.0096751, 0.21705, 0.47357],
          [0.0097347, 0.22308, 0.47746],
          [0.0098019, 0.22912, 0.48135],
          [0.0098809, 0.23521, 0.48525],
          [0.0099771, 0.24128, 0.48916],
          [0.010098, 0.24739, 0.49308],
          [0.010254, 0.25352, 0.49702],
          [0.010463, 0.25968, 0.50097],
          [0.010755, 0.26585, 0.50494],
          [0.011176, 0.27204, 0.50893],
          [0.011716, 0.2783, 0.51292],
          [0.012286, 0.28455, 0.51695],
          [0.012934, 0.29087, 0.521],
          [0.01379, 0.29721, 0.52507],
          [0.014838, 0.30358, 0.52918],
          [0.016131, 0.31002, 0.53331],
          [0.017711, 0.31647, 0.53748],
          [0.01963, 0.32299, 0.54168],
          [0.021948, 0.32955, 0.54593],
          [0.02473, 0.33614, 0.55021],
          [0.028047, 0.34283, 0.55454],
          [0.03198, 0.34954, 0.55891],
          [0.036812, 0.35633, 0.56334],
          [0.042229, 0.36317, 0.56781],
          [0.048008, 0.37009, 0.57235],
          [0.054292, 0.37708, 0.57693],
          [0.060963, 0.38413, 0.58157],
          [0.068081, 0.39126, 0.58628],
          [0.075457, 0.39846, 0.59104],
          [0.083246, 0.40574, 0.59587],
          [0.091425, 0.41309, 0.60075],
          [0.099832, 0.4205, 0.6057],
          [0.10859, 0.428, 0.61071],
          [0.11769, 0.43557, 0.61577],
          [0.12704, 0.44319, 0.6209],
          [0.1367, 0.45089, 0.62606],
          [0.14661, 0.45864, 0.63129],
          [0.15679, 0.46646, 0.63656],
          [0.16719, 0.47432, 0.64187],
          [0.17781, 0.48224, 0.64722],
          [0.18861, 0.49019, 0.6526],
          [0.19958, 0.49819, 0.65802],
          [0.21078, 0.5062, 0.66346],
          [0.22212, 0.51426, 0.66892],
          [0.2336, 0.52232, 0.6744],
          [0.24523, 0.53041, 0.67989],
          [0.257, 0.53852, 0.68541],
          [0.26887, 0.54662, 0.69091],
          [0.2808, 0.55472, 0.69643],
          [0.29285, 0.56282, 0.70193],
          [0.30498, 0.57091, 0.70745],
          [0.31717, 0.579, 0.71295],
          [0.32944, 0.58706, 0.71845],
          [0.34173, 0.59512, 0.72393],
          [0.35407, 0.60316, 0.72941],
          [0.36646, 0.61119, 0.73488],
          [0.37886, 0.61919, 0.74032],
          [0.39131, 0.62716, 0.74576],
          [0.40376, 0.63511, 0.75118],
          [0.41623, 0.64305, 0.75658],
          [0.42871, 0.65096, 0.76197],
          [0.4412, 0.65884, 0.76734],
          [0.4537, 0.6667, 0.7727],
          [0.4662, 0.67454, 0.77804],
          [0.4787, 0.68235, 0.78337],
          [0.49121, 0.69014, 0.78868],
          [0.50369, 0.69791, 0.79398],
          [0.51618, 0.70566, 0.79926],
          [0.52868, 0.71339, 0.80452],
          [0.54115, 0.72109, 0.80977],
          [0.55362, 0.72878, 0.81501],
          [0.5661, 0.73644, 0.82023],
          [0.57856, 0.74409, 0.82544],
          [0.59101, 0.75172, 0.83063],
          [0.60347, 0.75931, 0.83579],
          [0.61591, 0.7669, 0.84094],
          [0.62835, 0.77445, 0.84606],
          [0.64078, 0.78199, 0.85115],
          [0.6532, 0.78949, 0.85621],
          [0.66563, 0.79694, 0.86121],
          [0.67805, 0.80437, 0.86617],
          [0.69046, 0.81174, 0.87106],
          [0.70287, 0.81905, 0.87587],
          [0.71526, 0.82629, 0.88057],
          [0.72765, 0.83344, 0.88515],
          [0.74002, 0.84048, 0.88957],
          [0.75235, 0.84738, 0.89381],
          [0.76466, 0.85413, 0.89782],
          [0.77692, 0.86068, 0.90156],
          [0.7891, 0.86699, 0.90499],
          [0.80117, 0.87303, 0.90804],
          [0.81311, 0.87874, 0.91065],
          [0.82487, 0.88406, 0.91276],
          [0.8364, 0.88893, 0.9143],
          [0.84762, 0.89329, 0.9152],
          [0.85847, 0.89707, 0.91539],
          [0.86887, 0.90021, 0.91481],
          [0.87873, 0.90264, 0.91342],
          [0.88797, 0.9043, 0.91116],
          [0.8965, 0.90518, 0.90803],
          [0.90424, 0.90522, 0.90401],
          [0.91115, 0.90442, 0.89913],
          [0.91718, 0.9028, 0.89341],
          [0.92229, 0.90037, 0.88691],
          [0.92648, 0.89717, 0.87969],
          [0.92979, 0.89326, 0.87183],
          [0.93224, 0.8887, 0.8634],
          [0.93388, 0.88355, 0.85448],
          [0.93478, 0.87789, 0.84515],
          [0.93501, 0.8718, 0.83549],
          [0.93464, 0.86531, 0.82556],
          [0.93375, 0.85852, 0.81542],
          [0.93241, 0.85147, 0.80511],
          [0.93068, 0.84421, 0.79468],
          [0.92862, 0.83678, 0.78417],
          [0.9263, 0.82921, 0.77358],
          [0.92375, 0.82154, 0.76296],
          [0.92102, 0.8138, 0.75231],
          [0.91815, 0.806, 0.74166],
          [0.91516, 0.79816, 0.73101],
          [0.91208, 0.79029, 0.72037],
          [0.90893, 0.78242, 0.70975],
          [0.90574, 0.77454, 0.69915],
          [0.90251, 0.76667, 0.68859],
          [0.89925, 0.75881, 0.67805],
          [0.89597, 0.75097, 0.66755],
          [0.89269, 0.74315, 0.65709],
          [0.8894, 0.73535, 0.64666],
          [0.88612, 0.72757, 0.63627],
          [0.88283, 0.71983, 0.62592],
          [0.87956, 0.71211, 0.61562],
          [0.87629, 0.70442, 0.60536],
          [0.87303, 0.69676, 0.59514],
          [0.86978, 0.68914, 0.58497],
          [0.86655, 0.68154, 0.57483],
          [0.86333, 0.67398, 0.56475],
          [0.86012, 0.66645, 0.55471],
          [0.85692, 0.65896, 0.54471],
          [0.85373, 0.6515, 0.53475],
          [0.85056, 0.64406, 0.52484],
          [0.8474, 0.63667, 0.51497],
          [0.84426, 0.6293, 0.50515],
          [0.84113, 0.62196, 0.49537],
          [0.838, 0.61465, 0.48563],
          [0.83489, 0.60739, 0.47594],
          [0.8318, 0.60014, 0.46628],
          [0.82872, 0.59294, 0.45668],
          [0.82564, 0.58576, 0.44711],
          [0.82258, 0.5786, 0.43759],
          [0.81953, 0.57148, 0.42811],
          [0.8165, 0.56439, 0.41866],
          [0.81346, 0.55733, 0.40926],
          [0.81045, 0.55028, 0.39989],
          [0.80744, 0.54327, 0.39058],
          [0.80445, 0.53629, 0.3813],
          [0.80145, 0.52933, 0.37204],
          [0.79847, 0.52238, 0.36284],
          [0.7955, 0.51546, 0.35366],
          [0.79253, 0.50857, 0.34452],
          [0.78957, 0.50169, 0.33544],
          [0.78662, 0.49483, 0.32634],
          [0.78366, 0.48798, 0.31731],
          [0.7807, 0.48112, 0.3083],
          [0.77774, 0.4743, 0.29933],
          [0.77476, 0.46746, 0.29035],
          [0.77179, 0.46062, 0.28142],
          [0.76879, 0.45378, 0.27251],
          [0.76578, 0.44693, 0.26364],
          [0.76272, 0.44005, 0.25476],
          [0.75964, 0.43315, 0.24587],
          [0.75651, 0.4262, 0.23705],
          [0.75332, 0.41922, 0.22819],
          [0.75005, 0.41216, 0.21933],
          [0.7467, 0.40503, 0.21047],
          [0.74324, 0.39782, 0.20159],
          [0.73965, 0.39049, 0.19274],
          [0.7359, 0.38306, 0.18385],
          [0.73199, 0.37547, 0.17498],
          [0.72786, 0.36774, 0.16605],
          [0.72352, 0.35985, 0.15713],
          [0.71891, 0.35177, 0.14821],
          [0.71403, 0.3435, 0.13928],
          [0.70884, 0.33505, 0.13046],
          [0.70332, 0.32635, 0.12154],
          [0.69745, 0.3175, 0.11284],
          [0.69123, 0.30846, 0.10413],
          [0.68465, 0.29926, 0.095633],
          [0.67773, 0.28992, 0.08735],
          [0.67048, 0.28048, 0.079197],
          [0.6629, 0.27102, 0.07151],
          [0.65505, 0.26152, 0.064079],
          [0.64697, 0.25208, 0.057104],
          [0.63869, 0.24271, 0.050618],
          [0.63026, 0.23349, 0.04475],
          [0.62172, 0.22445, 0.039414],
          [0.61313, 0.21566, 0.034829],
          [0.60454, 0.20709, 0.031072],
          [0.59595, 0.19874, 0.028212],
          [0.5874, 0.1907, 0.026019],
          [0.57894, 0.18292, 0.024396],
          [0.57054, 0.17542, 0.023257],
          [0.56227, 0.16817, 0.022523],
          [0.55408, 0.1612, 0.02211],
          [0.54601, 0.1544, 0.021861],
          [0.53804, 0.14785, 0.021737],
          [0.53018, 0.14149, 0.021722],
          [0.52242, 0.13528, 0.0218],
          [0.51478, 0.12921, 0.021957],
          [0.50721, 0.12327, 0.022179],
          [0.49973, 0.11749, 0.022455],
          [0.49235, 0.11182, 0.022775],
          [0.48503, 0.10621, 0.02313],
          [0.4778, 0.10061, 0.023513],
          [0.47064, 0.095156, 0.023916],
          [0.46353, 0.089668, 0.024336],
          [0.45649, 0.084258, 0.024766],
          [0.44952, 0.078741, 0.025203],
          [0.4426, 0.073404, 0.025644],
          [0.43574, 0.067904, 0.026084],
          [0.42892, 0.062415, 0.026522],
          [0.42215, 0.056832, 0.026954],
          [0.41544, 0.051116, 0.027378],
          [0.40877, 0.045352, 0.02779],
          [0.40213, 0.039448, 0.028189],
          [0.39556, 0.033385, 0.02857],
          [0.38902, 0.027844, 0.028932],
          [0.3825, 0.022586, 0.029271],
          [0.37603, 0.017608, 0.029583],
          [0.36958, 0.01289, 0.029866],
          [0.36316, 0.0082428, 0.030115],
          [0.35679, 0.0040345, 0.030327],
          [0.35042, 6.1141e-05, 0.030499]]

# inverse vik (red to blue)
vik_inv_cm = vik_cm[::-1]

# hawaii
hawaii_cm = [[0.55054, 0.006842, 0.45198],
             [0.55149, 0.015367, 0.44797],
             [0.55243, 0.023795, 0.444],
             [0.55333, 0.032329, 0.44002],
             [0.55423, 0.04117, 0.43606],
             [0.5551, 0.049286, 0.43213],
             [0.55595, 0.056667, 0.42819],
             [0.5568, 0.063525, 0.42427],
             [0.55762, 0.06997, 0.42038],
             [0.55841, 0.076028, 0.41651],
             [0.55921, 0.081936, 0.41266],
             [0.55999, 0.087507, 0.40882],
             [0.56075, 0.092811, 0.40501],
             [0.56149, 0.098081, 0.40124],
             [0.56224, 0.10313, 0.39747],
             [0.56295, 0.108, 0.39374],
             [0.56366, 0.11287, 0.39002],
             [0.56435, 0.11753, 0.38634],
             [0.56503, 0.12212, 0.3827],
             [0.56571, 0.12668, 0.37907],
             [0.56638, 0.13117, 0.37547],
             [0.56704, 0.13554, 0.3719],
             [0.56768, 0.13987, 0.36838],
             [0.56831, 0.1442, 0.36486],
             [0.56894, 0.14842, 0.36138],
             [0.56956, 0.15262, 0.35794],
             [0.57017, 0.15681, 0.35452],
             [0.57078, 0.16093, 0.35113],
             [0.57138, 0.16501, 0.34776],
             [0.57197, 0.16912, 0.34442],
             [0.57256, 0.17313, 0.34112],
             [0.57314, 0.17717, 0.33784],
             [0.57371, 0.18114, 0.3346],
             [0.57428, 0.18515, 0.33136],
             [0.57484, 0.18909, 0.32817],
             [0.57541, 0.19304, 0.32499],
             [0.57597, 0.19698, 0.32185],
             [0.57652, 0.20085, 0.31874],
             [0.57706, 0.20478, 0.31565],
             [0.5776, 0.20866, 0.31256],
             [0.57814, 0.21254, 0.30954],
             [0.57868, 0.21643, 0.30652],
             [0.57921, 0.22029, 0.3035],
             [0.57975, 0.22411, 0.30052],
             [0.58027, 0.22798, 0.29757],
             [0.58079, 0.23182, 0.29462],
             [0.58131, 0.23565, 0.29171],
             [0.58183, 0.23946, 0.28881],
             [0.58235, 0.24327, 0.28591],
             [0.58287, 0.2471, 0.28307],
             [0.58339, 0.25092, 0.2802],
             [0.5839, 0.25474, 0.27738],
             [0.58442, 0.25853, 0.27455],
             [0.58493, 0.26234, 0.27174],
             [0.58544, 0.26616, 0.26898],
             [0.58595, 0.26997, 0.2662],
             [0.58646, 0.27377, 0.26344],
             [0.58696, 0.27758, 0.26068],
             [0.58747, 0.28137, 0.25793],
             [0.58797, 0.28518, 0.25522],
             [0.58848, 0.28901, 0.25249],
             [0.58898, 0.29282, 0.24977],
             [0.58949, 0.29665, 0.24708],
             [0.59, 0.30047, 0.24438],
             [0.59051, 0.3043, 0.24172],
             [0.59102, 0.30814, 0.23903],
             [0.59153, 0.31197, 0.23638],
             [0.59204, 0.31585, 0.23369],
             [0.59255, 0.3197, 0.23106],
             [0.59305, 0.32356, 0.22842],
             [0.59356, 0.32743, 0.22577],
             [0.59407, 0.33131, 0.22313],
             [0.59458, 0.33523, 0.22051],
             [0.5951, 0.33913, 0.21787],
             [0.59561, 0.34305, 0.21523],
             [0.59613, 0.34698, 0.21261],
             [0.59664, 0.35092, 0.20999],
             [0.59716, 0.35488, 0.20739],
             [0.59768, 0.35883, 0.20478],
             [0.5982, 0.36282, 0.20215],
             [0.59872, 0.36683, 0.19953],
             [0.59925, 0.37084, 0.19696],
             [0.59977, 0.37488, 0.19437],
             [0.60029, 0.37893, 0.19174],
             [0.60082, 0.38301, 0.18915],
             [0.60135, 0.38709, 0.18655],
             [0.60187, 0.39122, 0.18395],
             [0.6024, 0.39535, 0.18134],
             [0.60293, 0.39949, 0.17878],
             [0.60346, 0.40368, 0.17616],
             [0.604, 0.40787, 0.17359],
             [0.60452, 0.4121, 0.17101],
             [0.60504, 0.41635, 0.16844],
             [0.60556, 0.42062, 0.16585],
             [0.60608, 0.42493, 0.16332],
             [0.60661, 0.42925, 0.16073],
             [0.60713, 0.4336, 0.1582],
             [0.60764, 0.438, 0.15565],
             [0.60814, 0.44241, 0.15309],
             [0.60864, 0.44685, 0.15058],
             [0.60913, 0.45132, 0.14807],
             [0.60961, 0.45583, 0.14561],
             [0.61008, 0.46036, 0.14312],
             [0.61054, 0.46493, 0.14069],
             [0.61099, 0.46954, 0.13827],
             [0.61142, 0.47417, 0.13583],
             [0.61183, 0.47884, 0.13351],
             [0.61223, 0.48354, 0.13121],
             [0.6126, 0.48829, 0.12892],
             [0.61295, 0.49305, 0.12672],
             [0.61327, 0.49788, 0.12457],
             [0.61357, 0.5027, 0.12249],
             [0.61384, 0.50759, 0.12051],
             [0.61407, 0.5125, 0.11867],
             [0.61426, 0.51746, 0.11685],
             [0.61442, 0.52243, 0.11516],
             [0.61453, 0.52746, 0.11366],
             [0.61459, 0.53251, 0.11227],
             [0.61461, 0.53759, 0.11103],
             [0.61457, 0.54271, 0.11],
             [0.61447, 0.54785, 0.10911],
             [0.61431, 0.55302, 0.10842],
             [0.61408, 0.55821, 0.10801],
             [0.61379, 0.56345, 0.10785],
             [0.61342, 0.56868, 0.10794],
             [0.61297, 0.57395, 0.10831],
             [0.61245, 0.57923, 0.10903],
             [0.61184, 0.58452, 0.11004],
             [0.61115, 0.58982, 0.11132],
             [0.61035, 0.59513, 0.11296],
             [0.60947, 0.60044, 0.11486],
             [0.60849, 0.60575, 0.11717],
             [0.60741, 0.61106, 0.11981],
             [0.60622, 0.61635, 0.12276],
             [0.60493, 0.62162, 0.12612],
             [0.60354, 0.62688, 0.12976],
             [0.60203, 0.63211, 0.13369],
             [0.60041, 0.63731, 0.13797],
             [0.59869, 0.64247, 0.1425],
             [0.59686, 0.64759, 0.14733],
             [0.59492, 0.65266, 0.15242],
             [0.59287, 0.6577, 0.15779],
             [0.59071, 0.66267, 0.16342],
             [0.58844, 0.66758, 0.16926],
             [0.58608, 0.67243, 0.17528],
             [0.58361, 0.67721, 0.18151],
             [0.58105, 0.68192, 0.18799],
             [0.57839, 0.68656, 0.19459],
             [0.57565, 0.69112, 0.20131],
             [0.57281, 0.69561, 0.20824],
             [0.56988, 0.70002, 0.21528],
             [0.56689, 0.70435, 0.22247],
             [0.56381, 0.7086, 0.22974],
             [0.56066, 0.71275, 0.23717],
             [0.55746, 0.71684, 0.24462],
             [0.55418, 0.72084, 0.25222],
             [0.55085, 0.72477, 0.25987],
             [0.54747, 0.7286, 0.26757],
             [0.54404, 0.73238, 0.27539],
             [0.54057, 0.73606, 0.28324],
             [0.53707, 0.73968, 0.29114],
             [0.53351, 0.74323, 0.29909],
             [0.52994, 0.7467, 0.30708],
             [0.52633, 0.75011, 0.31511],
             [0.5227, 0.75346, 0.32319],
             [0.51905, 0.75675, 0.33128],
             [0.51537, 0.75998, 0.33944],
             [0.51168, 0.76316, 0.3476],
             [0.50799, 0.76629, 0.35578],
             [0.50428, 0.76937, 0.36398],
             [0.50055, 0.77241, 0.37222],
             [0.49682, 0.77541, 0.38048],
             [0.49309, 0.77836, 0.38876],
             [0.48935, 0.78129, 0.39705],
             [0.48561, 0.78418, 0.40538],
             [0.48188, 0.78704, 0.41371],
             [0.47814, 0.78987, 0.42206],
             [0.47441, 0.79267, 0.43044],
             [0.47068, 0.79546, 0.43882],
             [0.46695, 0.79822, 0.44724],
             [0.46322, 0.80096, 0.45567],
             [0.45952, 0.80369, 0.46412],
             [0.45581, 0.80641, 0.47258],
             [0.45212, 0.80911, 0.48105],
             [0.44844, 0.8118, 0.48955],
             [0.44477, 0.81447, 0.49809],
             [0.44111, 0.81714, 0.50662],
             [0.43749, 0.8198, 0.51517],
             [0.43386, 0.82246, 0.52375],
             [0.43028, 0.82511, 0.53235],
             [0.42672, 0.82776, 0.54096],
             [0.42319, 0.8304, 0.5496],
             [0.41971, 0.83304, 0.55824],
             [0.41626, 0.83567, 0.56692],
             [0.41287, 0.83831, 0.57561],
             [0.40952, 0.84094, 0.58431],
             [0.40624, 0.84356, 0.59304],
             [0.40304, 0.84619, 0.60178],
             [0.3999, 0.84882, 0.61054],
             [0.39687, 0.85144, 0.61932],
             [0.39395, 0.85406, 0.6281],
             [0.39115, 0.85668, 0.63691],
             [0.38847, 0.8593, 0.64571],
             [0.38593, 0.86192, 0.65453],
             [0.38359, 0.86453, 0.66337],
             [0.38141, 0.86713, 0.6722],
             [0.37942, 0.86973, 0.68102],
             [0.37767, 0.87232, 0.68986],
             [0.37617, 0.87491, 0.69869],
             [0.37492, 0.87748, 0.70751],
             [0.37398, 0.88005, 0.71632],
             [0.37334, 0.8826, 0.72511],
             [0.37304, 0.88514, 0.73387],
             [0.37311, 0.88765, 0.7426],
             [0.37357, 0.89016, 0.7513],
             [0.37444, 0.89264, 0.75995],
             [0.37572, 0.8951, 0.76855],
             [0.37747, 0.89752, 0.7771],
             [0.37967, 0.89992, 0.78557],
             [0.38235, 0.90229, 0.79397],
             [0.38553, 0.90462, 0.80228],
             [0.38921, 0.90691, 0.8105],
             [0.39338, 0.90916, 0.81862],
             [0.39807, 0.91137, 0.82663],
             [0.40326, 0.91353, 0.83451],
             [0.40893, 0.91563, 0.84226],
             [0.41508, 0.91769, 0.84986],
             [0.4217, 0.91968, 0.85731],
             [0.42879, 0.92161, 0.86461],
             [0.43631, 0.92349, 0.87173],
             [0.44423, 0.92529, 0.87868],
             [0.45254, 0.92703, 0.88545],
             [0.4612, 0.9287, 0.89204],
             [0.47021, 0.93031, 0.89842],
             [0.47952, 0.93184, 0.90462],
             [0.4891, 0.9333, 0.91062],
             [0.49895, 0.93469, 0.91641],
             [0.50902, 0.936, 0.92201],
             [0.51928, 0.93725, 0.92739],
             [0.52972, 0.93842, 0.93259],
             [0.54029, 0.93952, 0.93759],
             [0.551, 0.94055, 0.9424],
             [0.5618, 0.94151, 0.94702],
             [0.57269, 0.94241, 0.95146],
             [0.58362, 0.94324, 0.95573],
             [0.59461, 0.94402, 0.95982],
             [0.60561, 0.94473, 0.96377],
             [0.61664, 0.94539, 0.96756],
             [0.62765, 0.94599, 0.97121],
             [0.63865, 0.94654, 0.97474],
             [0.64962, 0.94705, 0.97815],
             [0.66055, 0.94751, 0.98145],
             [0.67144, 0.94793, 0.98465],
             [0.68228, 0.94832, 0.98777],
             [0.69306, 0.94866, 0.9908],
             [0.70378, 0.94898, 0.99377]]

# make the list of colormap

my_colormaps: list[ColorMap] = []

# EVERY TIME YOU ADD A COLORMAP: add to 'names' AND raw_colormaps
names = ['greyscale', 'batlow', 'vik', 'vik_inv', 'hawaii']
raw_colormaps = [grey, batlow_cm, vik_cm, vik_inv_cm, hawaii_cm]

for i, colormap in enumerate(raw_colormaps):
    formatted_colormap = np.rint(np.array(colormap)*255).astype(int)
    ticks = np.linspace(0., 1., len(formatted_colormap))
    my_colormaps.append(ColorMap(ticks, formatted_colormap, name=names[i]))

gradients_names = list(Gradients.keys())
for name, value in Gradients.items():
    # remove the gradients that are not rgb because they are not supported
    # by pyqtgraph.GradientEditorItem
    if "mode" not in value:
        gradients_names.pop(gradients_names.index(name))
    elif value["mode"] != "rgb":
        gradients_names.pop(gradients_names.index(name))
    if name == "grey":
        gradients_names.pop(gradients_names.index(name))

for name in gradients_names:
    my_colormaps.append(ColorMap(*zip(*Gradients[name]["ticks"]), name=name))

my_cyclic_colormaps: list[ColorMap] = []
for cmap in my_colormaps:
    my_cyclic_colormaps.append(ColorMap(*cmap.getStops(), mapping=ColorMap.REPEAT,
                                        name="cyclic "+cmap.name))


def create_colormap_texture(colormap: ColorMap) -> QOpenGLTexture:
    """
    A valid OpenGL context must be made current before this function is called.
    Transform a pyqtgraph ColorMap into an OpenGL TEXTURE_1D. If the stops of the colormap are not
    evenly spaced, an evenly spaced lookup table is created by interpolating between stops.

    Parameters
    ----------
    colormap : pyqt graph ColorMap
        The colormap to transform into texture

    Returns
    -------
    int
        The identifier of the created texture.
    """
    stops, _ = colormap.getStops()
    # stopsdiffs are the steps between stops
    stopsdiffs = np.diff(stops)
    # test if stops are evenly spaced (the diff between two stops is always approximately
    # the same than the first diff)
    if not all(np.isclose(stopsdiffs, stopsdiffs[0])):
        # if stops are not evenly spaced then we create an evenly spaced lookup table
        # by interpolating linearly between stops
        colormap_colors = colormap.getLookupTable(nPts=256, alpha=False, mode=ColorMap.FLOAT
                                                  ).astype(np.float32)
    else:
        # else we use the colors associated to stops (without the fourth alpha chanel)
        colormap_colors = colormap.getColors(mode=ColorMap.FLOAT)[:, :3].astype(np.float32)
    w, _ = colormap_colors.shape
    colormap_colors = np.array([colormap_colors], dtype=np.float32)
    texture = QOpenGLTexture(QOpenGLTexture.Target.Target1D)
    texture.setMagnificationFilter(QOpenGLTexture.Filter.Linear)
    texture.setMinificationFilter(QOpenGLTexture.Filter.Linear)
    if colormap.mapping_mode == ColorMap.CLIP:
        texture.setWrapMode(QOpenGLTexture.WrapMode.ClampToEdge)
    elif colormap.mapping_mode == ColorMap.REPEAT:
        texture.setWrapMode(QOpenGLTexture.WrapMode.Repeat)
    elif colormap.mapping_mode == ColorMap.MIRROR:
        texture.setWrapMode(QOpenGLTexture.WrapMode.MirroredRepeat)
    else:
        raise ValueError(f"colormap.wrapping_mode not supported {colormap.wrapping_mode}")
    texture.setSize(w)
    texture.setFormat(QOpenGLTexture.TextureFormat.RGB32F)
    texture.allocateStorage(QOpenGLTexture.PixelFormat.RGB, QOpenGLTexture.PixelType.Float32)
    texture.setData(QOpenGLTexture.PixelFormat.RGB, QOpenGLTexture.PixelType.Float32,
                    colormap_colors)
    texture.generateMipMaps()
    assert texture.textureId(), "Colormap : cannot create texture in OpenGl"
    return texture
